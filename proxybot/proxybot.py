import logging
import re
import urllib
import time
from datetime import datetime, timedelta

import i18n
import lxml.html
from mastodon import StreamListener, \
    MastodonNetworkError, MastodonNotFoundError

import proxybot.network
import proxybot.table
import proxybot.toot

def remove_html(text):
    """Removes the html tags from a given text.
    
    All the mentions are expanded in their complete form (e.g. @name@domain). 
    Paragraphs and <br> tags are replaced by newlines.
    """
    html = lxml.html.fromstring(text)
    lxml.etree.strip_tags(html, 'span')
    
    mentions = html.cssselect('a.mention')
    for mention in mentions:
        domain = urllib.parse.urlsplit(mention.get('href')).netloc
        mention.text = mention.text + '@' + domain
    
    for node in html.cssselect('br, p'):
        newlines = '\n'
        if node.tag == 'p':
            newlines += '\n'
        
        try:
            node.tail = newlines + node.tail
        except TypeError:
            node.tail = newlines
    
    text = html.text_content().strip()
    
    return text

def remove_mentions(message):
    """Removes mentions at the beginning of a given message"""
    regex = proxybot.toot.RE_MENTION
    while regex.match(message):
        message = regex.sub('', message, 1).lstrip()
    
    return message

def disable_mentions(message, symbol = '&'):
    """Disable all the mentions in a given message.
    
    The first "@" in each mention is replaced by another symbol.
    """
    return proxybot.toot.RE_MENTION.sub(rf'\1{symbol}\2\3', message)

def enable_mentions(message, symbol = '&'):
    """Enable all the mentions in a given message.
    
    The first symbol in each mention is replaced by the "@" character.
    """
    re_mention = proxybot.toot.RE_MENTION
    pattern = re_mention.pattern.replace('@', symbol, 1)
    return re.sub(pattern, r'\1@\2\3', message, flags=re_mention.flags)

class Command:
    """Abstract class representing a user command. 
    
    The static attribute REGEX checks if the command is called by the user in 
    his toot.
    
    The method `execute` contains the actual behavior of the command. Thus, it
    must be implemented by the subclasses.
    """
    REGEX = None
    
    def __init__(self, db, toot):
        self.toot = toot
    
    @staticmethod
    def _ancestors(toot):
        """Generates the list of ancestor toots for a given toot.
        
        An ancestor toot is a toot posted before a given toot and in the same
        thread of that toot.
        
        The returned list is in the ascendant order, that is, from the most recent to the oldest ancestor.
        
        If there is no ancestors, then raises a MastodonNotFoundError.
        """ 
        if toot.in_reply_to_id is None:
            raise MastodonNotFoundError
        
        context = proxybot.network.mastodon.status_context(toot)
        
        return reversed(context.ancestors)
    
    def execute(self):
        """Executes the command.
        
        This method represents the actual behavior of the command class.
        """
        raise NotImplementedError
    
class Help(Command):
    """Sends a toot with some help information to the user.
    """
    REGEX = re.compile(r'!help', re.IGNORECASE)
    
    def execute(self):
        logging.info(f"{self.toot.url} - Showing help")

        reply = proxybot.toot.make_reply(
            i18n.t('command.help.message'), self.toot)
        reply.send()
    
class Toot(Command):
    """Forward the content of a given toot.
    """
    REGEX = re.compile(r'!toot', re.IGNORECASE)
    
    def execute(self):
        message = remove_html(self.toot.content)
        message = remove_mentions(message)
        message = enable_mentions(message)
        
        mastodon_config = proxybot.config.parser['mastodon']
        
        reply = proxybot.toot.Toot(message,
            spoiler_text=self.toot.spoiler_text,
            visibility=mastodon_config.get('visibility'))
        reply.send()
        
class Reply(Command):
    """Sends a reply to a notification received from an user.
    
    Each time an user mention the bot, the message will be forwarded to the 
    admins. They then have the possibility to reply to that toot with the !
    reply command.
    """
    REGEX = re.compile(r'!reply', re.IGNORECASE)
    
    def __init__(self, db, toot):
        super().__init__(db, toot)
        
        self.toots = proxybot.table.Toots(db)
    
    def _find_user_toot(self, toot):
        """Find the original toot sent by the user to the bot.
        
        If the toot is not found, raises a MastodonNotFoundError
        """
        mastodon = proxybot.network.mastodon
        
        # Look in the thread after the toot forwarded by the bot
        for ancestor in self._ancestors(toot):
            if ancestor.account.acct != mastodon.user.acct:
                continue
            
            try:
                # Once we found a toot from the bot, look in the database to 
                # see if it was a forwarded toot
                user_toot_id = self.toots[ancestor]
            except KeyError:
                continue
            
            # Retrieves the toot sent by the user
            return mastodon.status(user_toot_id)
        
        raise MastodonNotFoundError
        
    def execute(self):
        try:
            user_toot = self._find_user_toot(self.toot)
        except MastodonNotFoundError:
            logging.warn(f"{self.toot.url} - "
                "Cannot find the toot sent by the user to the bot")
            
            reply = proxybot.toot.make_reply(
                i18n.t(f"command.reply.not_found"), self.toot)
            reply.send()
            return
        
        message = remove_html(self.toot.content)
        message = remove_mentions(message)
        message = enable_mentions(message)
        
        reply = proxybot.toot.make_reply(message, user_toot)
        reply.send()
        
class Notify(Command):
    """Notifies the admins of an user's message.
    
    Each time an user mentions the bot, the message is forwarded to the admins.
    """
    def __init__(self, db, toot):
        super().__init__(db, toot)
        
        self.toots = proxybot.table.Toots(db)
    
    def _find_bot_toot(self, toot):
        """Finds the toot sent by the bot to notify the admins of a user 
        message.
        
        If the toot is not found, raises a MastodonNotFoundError
        """
        if toot is None:
            raise MastodonNotFoundError
        
        try:
            bot_toot_id = self.toots[toot]
        except KeyError:
            raise MastodonNotFoundError
        
        return proxybot.network.mastodon.status(bot_toot_id)
        
    def execute(self):
        message = remove_html(self.toot.content)
        message = remove_mentions(message)
        message = disable_mentions(message)
        
        author = self.toot.account.acct
        if '@' not in author: # local username -> no domain in the account
            mastodon_config = proxybot.config.parser['mastodon']
            
            instance_url = mastodon_config['instance']
            instance_domain = urllib.parse.urlsplit(instance_url).netloc
            
            author += '@' + instance_domain
        
        message = i18n.t('command.notify.wrote').format(
            author=author, message=message)
        
        try:
            in_reply_to = self._find_bot_toot(self.toot.in_reply_to_id)
        except MastodonNotFoundError:
            in_reply_to = self.toot
        
        admins = proxybot.config.parser['mastodon']['admins']
        
        reply = proxybot.toot.Toot(message, in_reply_to,
            mentions=admins, spoiler_text=self.toot.spoiler_text,
            visibility='direct')
        bot_toots = reply.send()
        
        for bot_toot in bot_toots:
            self.toots[bot_toot] = self.toot

class Reaction(Command):
    """Abstract class representing a reaction command.
    
    A reaction command is either a fav or a boost, or their inverse (i.e. unfav, unboost).
    """
    def __init__(self, db, reaction_type, toot, filters, force = None):
        super().__init__(db, toot)
        
        self.filters = filters
        self.reaction_type = reaction_type
        
        if isinstance(force, bool):
            self.force = force
        else:
            self.force = force is not None
    
    def _find_target(self, toot):
        """Finds a parent toot which meets the criteria defined by the filter functions.
        
        If any of the filter function doesn't return true, then the toot is ignored and its parent is checked, etc.
        
        If no parent toot meets the criteria, then raise a MastodonNotFoundError.
        """
        for ancestor in self._ancestors(toot):
            if self.toot.account.acct == proxybot.network.mastodon.user.acct:
                continue
            
            if not all(function(ancestor) for function in self.filters):
                continue
            
            return ancestor
        
        raise MastodonNotFoundError
    
    def execute(self):
        reaction = self.reaction_type
        
        try:
            target = self._find_target(self.toot)
        except MastodonNotFoundError:
            logging.warn(f"{self.toot.url}"
                f" - Cannot find the target toot to {reaction}.")
                
            reply = proxybot.toot.make_reply(
                i18n.t(f"command.{reaction}.not_found"), self.toot)
            reply.send()
            return
        
        if self.force:
            self._unreact(target)
        elif self.has_reacted(target):
            logging.warn(f"{self.toot.url}"
                f" - Cannot {reaction} the target: toot already {reaction}ed")
            
            reply = proxybot.toot.make_reply(
                i18n.t(f"command.{reaction}.already_reacted"), self.toot)
            reply.send()
            return
        
        self._react(target)
        
        reply = proxybot.toot.make_reply(
            i18n.t(f"command.{reaction}.message"), self.toot)
        reply.send()
    
    def has_reacted(self, toot):
        """Indicates whether the toot was already reacted or not."""
        raise NotImplementedError
    
    def _react(self, toot):
        """Encapsulates the Mastodon action to use for the reaction."""
        raise NotImplementedError
    
    def _unreact(self, toot):
        """Encapsulates the Mastodon action to use for the inverse of the reaction."""
        raise NotImplementedError

class UnReaction(Reaction):
    """Represents the inverse of a reaction.
    """
    def __init__(self, db, toot):
        super().__init__(db, toot, force=False)
        
        self.reaction_type = 'un' + self.reaction_type
    
    def has_reacted(self, toot):
        return not super().has_reacted(toot)
    
    def _react(self, toot):
        return super()._unreact(toot)
    
    def _unreact(self, toot):
        return super()._react(toot)

class Boost(Reaction):
    """Boosts a parent of a given toot.
    
    The parent toot must be boostable (i.e. not private).
    """
    REGEX = re.compile(r'!boost (force)?', re.IGNORECASE)
    
    def __init__(self, db, toot, force = None):
        filters = (lambda toot: toot.visibility in ('public', 'unlisted'),)
        super().__init__(db, 'boost', toot, filters, force)
    
    def has_reacted(self, toot):
        return toot.reblogged
    
    def _react(self, toot):
        return proxybot.network.mastodon.status_reblog(toot)
    
    def _unreact(self, toot):
        return proxybot.network.mastodon.status_unreblog(toot)

class UnBoost(UnReaction, Boost):
    """Unboost a parent of a given toot.
    """
    REGEX = re.compile(r'!unboost (force)?', re.IGNORECASE)
        
class Fav(Reaction):
    """Add a parent of a given toot to the bot's favourites.
    
    The parent toot must be from a user (i.e. not from admins nor the bot).
    """
    REGEX = re.compile(r'!fav (force)?', re.IGNORECASE)
    
    def __init__(self, db, toot, force = False):
        admins = proxybot.config.parser['mastodon']['admins']
        filters = (lambda toot: toot.account.acct not in admins,)
        super().__init__(db, 'fav', toot, filters, force)
    
    def has_reacted(self, toot):
        return toot.favourited
    
    def _react(self, toot):
        return proxybot.network.mastodon.status_favourite(toot)
    
    def _unreact(self, toot):
        return proxybot.network.mastodon.status_unfavourite(toot)

class UnFav(UnReaction, Fav):
    """Unfav a parent of a given toot.
    """
    REGEX = re.compile(r'!unfav (force)?', re.IGNORECASE)

class ProxyListener(StreamListener):
    def __init__(self):
        self.db = proxybot.table.connect()
        self.commands = (Help, Toot, Reply, Boost, UnBoost, Fav, UnFav)
    
    def __enter__(self):
        return self
    
    def __exit__(self, *exc):
        self.db.close()
    
    def on_notification(self, notification):
        if notification.type != 'mention':
            return

        toot = notification.status
        
        cmd = self._find_command(toot)
        
        if cmd is None:
           return
        
        try:
            cmd.execute()
        except Exception as e:
            logging.critical(f"{toot.url} - "
                "An error occurred, notification couldn't be processed",
                exc_info=e)

    def _find_command(self, toot):
        admins = proxybot.config.parser['mastodon']['admins']
        
        for cmd in self.commands:
            regex = cmd.REGEX
            
            match = regex.search(toot.content)
            if not match:
                continue
            
            if toot.account.acct not in admins:
                logging.warn(f"{toot.url} - Only admins can execute commands.")
                return
            
            args = match.groups()
            
            # remove the command from the toot content
            toot['content'] = regex.sub('', toot.content)
            
            return cmd(self.db, toot, *args)
        
        if toot.account.acct in admins:
            logging.warn(f"{toot.url} - Cannot parse the command")
            return
        
        return Notify(self.db, toot)
    
def main():
    with ProxyListener() as listener:
        proxybot.network.stream(listener)

    logging.info("Shutting down…")

if __name__ == '__main__':
   main()
