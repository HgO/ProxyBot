CREATE TABLE IF NOT EXISTS `toots` (
    user_toot INT NOT NULL,
    bot_toot INT NOT NULL,
    date INT NOT NULL,
    CONSTRAINT key_toot PRIMARY KEY (user_toot, bot_toot)
);
