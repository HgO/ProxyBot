import collections
import itertools
import logging 
import re
import uuid

from mastodon import MastodonNetworkError, MastodonNotFoundError
from twitter_text.regex import REGEXEN

import proxybot.network

RE_URL = re.compile(r'(https?:\/\/)' \
    + REGEXEN['valid_domain'].pattern \
    + '(?::' + REGEXEN['valid_port_number'].pattern + ')?' \
    + '(/' + REGEXEN['valid_url_path'].pattern + '*)?' \
    + '(\?' + REGEXEN['valid_url_query_chars'].pattern + '*' \
    + REGEXEN['valid_url_query_ending_chars'].pattern + ')?')
RE_MENTION = re.compile(
    r'(^|[^/\w])@([\w]+)(@[\w\.\-]+[a-z0-9]+)?', re.IGNORECASE)

RE_WHITESPACES = re.compile(r'\s+')

class Toot:
    """Wrapper for sending a toot.
    
    Each `Toot` object has an unique identifier, which is used as an 
    idempotency key (to avoid sending duplicate toots).
    """
    def __init__(self, message, in_reply_to = None, mentions = [],
            visibility = None, spoiler_text = None):
        self.id = str(uuid.uuid4())
        
        self.message = message
        self.in_reply_to = in_reply_to
        self.mentions = mentions
        self.visibility = visibility
        self.spoiler_text = spoiler_text
        
    def send(self, max_chunks = None):
        """Send the toot to mastodon.
        
        If the size of the toot is bigger than max_toot_size, then the message 
        is split into several chunks. If max_chunks is specified, then the 
        message is truncated. For instance, max_chunks = 1 will send one toot 
        with a truncated message.
        
        When the toot referred by the attribute `in_reply_to` doesn't exist, we 
        fallback to sending a standalone toot as-if it was a reply (i.e. the 
        user is still mentionned but in a new thread).
        
        This method raises a MastodonNetworkError in case of network issues.
        
        Returns the dict of each toot sent.
        """
        mentions = ''.join(f'@{mention} ' for mention in self.mentions)
        
        mastodon_config = proxybot.config.parser['mastodon']
        max_toot_size = mastodon_config['max_toot_size'] - len(mentions)
        
        in_reply_to = self.in_reply_to
        
        if max_chunks is not None and max_chunks < 0:
            max_chunks = None
        
        chunks = chunk(self.message, max_toot_size)
        
        sent_toots = []
        for message in itertools.islice(chunks, max_chunks):
            try:
                in_reply_to = self._send_chunk(mentions + message, in_reply_to)
            except MastodonNetworkError as e:
                try:
                    error_message = f"{self.in_reply_to.url} - "
                except KeyError:
                    error_message = ''
                error_message += "Cannot send toot due to network issues"
                
                logging.critical(error_message, exc_info=e)
                break
            
            sent_toots.append(in_reply_to)
            
        return sent_toots
    
    def _send_chunk(self, message, in_reply_to = None):
        """Send one toot chunk to mastodon.
        
        When the toot referred by the attribute `in_reply_to` doesn't exist, we 
        fallback to sending a standalone toot as-if it was a reply (i.e. the 
        user is still mentionned but in a new thread).
        
        This method raises a MastodonNetworkError in case of network issues.
        
        Returns the dict of the toot sent.
        """
        idempotency_key = self.id
        
        if in_reply_to is not None:
            idempotency_key += str(in_reply_to.id)
        
        try:
            return proxybot.network.mastodon.status_post(message, 
                in_reply_to_id=in_reply_to,
                visibility=self.visibility, 
                spoiler_text=self.spoiler_text,
                idempotency_key=idempotency_key)
        except MastodonNotFoundError:
            try:
                logging.warn(f"{in_reply_to.url} - "
                    "Cannot send a reply because the toot doesn't exist")
            except AttributeError:
                pass
            
        return proxybot.network.mastodon.status_post(message,
            visibility=self.visibility,
            spoiler_text=self.spoiler_text,
            idempotency_key=idempotency_key)
    
def make_reply(message, in_reply_to,
        untag = True, visibility = None, spoiler_text = None):
    """Creates a reply to a toot. 
    
    Mentions are kept in the same order as they appear in the message. The 
    function also keeps the visibility and spoiler text of the toot to which 
    we want to reply.
    
    If untag is True, then only mentions the user we are replying to, and 
    removes every other mentionned users from the conversation.
    
    This function is actually a wrapper of `Mastodon.status_reply`.
    """
    user_id = proxybot.network.mastodon.user.id
    
    if visibility is None:
        visibility = in_reply_to.visibility
    if spoiler_text is None:
        spoiler_text = in_reply_to.spoiler_text
    
    mentions = collections.OrderedDict()
    
    if in_reply_to.account.id != user_id:
        mentions[in_reply_to.account.id] = in_reply_to.account.acct
    
    if not untag:
        for mention in in_reply_to.mentions:
            if mention.id != user_id:
                mentions[mention.id] = mention.acct
    
    return Toot(message, in_reply_to, mentions=list(mentions.values()), 
        visibility=visibility, spoiler_text=spoiler_text)

def length(message):
    """Computes the length of a toot message.
    
    This function replicates the algorithm used by Mastodon to compute the 
    length of a toot. More specifically, each url always has a length of 23 and
    domains in the mentions are not taken into account.
    """
    message = RE_URL.sub('x' * 23, message)
    
    # 1st group is the space before the mention
    # 2nd group is the username without the "@"
    message = RE_MENTION.sub('\1@\2', message)
    
    return len(message)

def chunk(text, chunk_size, ellipsis = '…'):
    """Create a given text into chunks of a certain size.
    
    When possible, the text is split as soon as we meet a blank character.
    Otherwise, the token is split into several parts until it can fit a chunk.
    
    The ellipsis is added at the end of every chunk except the last one. It can 
    be used to indicate that the message has been split into parts.
    """
    text += ' ' # dummy space at the end to make the algorithm simpler

    chunk_size -= len(ellipsis)
    
    text_size = 0 # size of the text according to Mastodon
    sep_size = 0 # size of the last separator (here: 0 or 1)
    
    start = 0 # start index of the current chunk
    token_start = 0 # start index of the current token
    
    for match in RE_WHITESPACES.finditer(text):
        token_end = match.start()
        
        token = text[token_start:token_end]
        token_size = length(token)
        
        text_size += sep_size + token_size
        
        # Text is bigger than a chunk, we must split it…
        if text_size > chunk_size:
            # Text must be split on the previous token
            if token_size <= chunk_size or text_size-token_size >= chunk_size:
                diff_size = len(token) + sep_size
                
                yield text[start:token_end-diff_size] + ellipsis
                
                start = token_start
                text_size = token_size
            
            # Current token is too long, must be split into several chunks
            while token_size > chunk_size:
                diff_size = text_size - chunk_size
                
                yield text[start:token_end-diff_size] + ellipsis
                
                token_size = diff_size
                
                start = token_end - diff_size
                text_size = token_size
        
        token_start = match.end()
        sep_size = len(match.group())
    
    yield text[start:-1]
