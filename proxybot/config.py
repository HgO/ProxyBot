import os
import configparser
import locale
import logging
import re

import i18n

# we find the config.ini file in the current directory by default
CONFIG_FILENAME = os.path.join(os.getcwd(), 'config.ini')

BASE_DIR = os.path.dirname(__file__)
LOCALES_DIR = os.path.join(BASE_DIR, 'locales')
DB_SCHEMAS_FILENAME = os.path.join(BASE_DIR, 'schemas.sql')

def init(filename = CONFIG_FILENAME):
    # config dir may be different that parbot dir if you don't clone the repo
    config_dir = os.path.dirname(os.path.abspath(filename))

    parser = configparser.RawConfigParser()
    parser.read(filename)

    # Relative filenames
    paths = [
        ('mastodon', 'access_token', 'token.secret'),
        ('sqlite3', 'database', 'proxybot.db'),
        ('log', 'path', 'proxybot.log')
    ]
    for section, key, default in paths:
        path = parser[section].get(key, default)

        if not os.path.isabs(path):
            parser[section][key] = os.path.join(config_dir, path)

    db_config = parser['sqlite3']
    schemas_filename = db_config.get('schemas', DB_SCHEMAS_FILENAME)
    db_config['schemas'] = os.path.abspath(schemas_filename)

    # Logs
    log_config = parser['log']
    log_config['level'] = log_config.get('level', 'info')

    logging.basicConfig(filename=parser['log']['path'],
        level=getattr(logging, log_config['level'].upper()),
        format='%(asctime)s - %(levelname)s - %(message)s')
    
    # Mastodon
    mastodon_config = parser['mastodon']
    
    admins = re.split(r'[\s,]+', mastodon_config['admins'].strip())
    parser.set('mastodon', 'admins', admins)
    
    try:
        max_toot_size = parser.getint('mastodon', 'max_toot_size')
    except configparser.NoOptionError:
        max_toot_size = 500
    parser.set('mastodon', 'max_toot_size', max_toot_size)
    
    # default parameters
    default_config = parser.defaults()

    # Translations
    locale_name = default_config.get('lang', os.environ.get('LANG'))
    lang, *country = locale_name.split('_')

    if not len(country):
        locale_name, encoding = locale.normalize(lang).split('.')
        encoding = locale.getpreferredencoding()
        locale_name += '.' + encoding

    locale.setlocale(locale.LC_ALL, locale_name)
    i18n.set('locale', lang)
    i18n.set('fallback', 'en')

    locales_dir = os.path.abspath(default_config.get('locales', LOCALES_DIR))
    i18n.load_path.append(locales_dir)
    i18n.set('file_format', 'json')
    i18n.set('filename_format', '{locale}.{format}')

    return parser

parser = init(os.environ.get("CONFIG_FILE", CONFIG_FILENAME))
