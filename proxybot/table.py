import contextlib
import sqlite3
from datetime import datetime

from mastodon import MastodonError

import proxybot.config
import proxybot.network

def connect(load_schemas = True, **flags):
    """Establishes a new connection to the sqlite3 database.
    
    When `load_schemas` is True, loads the initialization schemas.
    
    Returns the sqlite3 database.
    """
    
    database_config = proxybot.config.parser['sqlite3']
    
    db = sqlite3.connect(database_config['database'], **flags)
    db.execute("PRAGMA foreign_keys = ON")
    
    if not load_schemas:
        return db
    
    with open(database_config['schemas'], 'r') as ifs:
        with contextlib.closing(db.cursor()) as cursor:
            cursor.executescript(ifs.read())
    
    return db

class Table:
    def __init__(self, db):
        self.db = db

class Toots(Table):
    """This table stores the toots that have been notified to the admin.
    """
    def __setitem__(self, bot_toot, user_toot):
        """Syntactic sugar for adding a new pair of toots to the table.
        """
        self.add(bot_toot, user_toot)
        
    def __getitem__(self, toot):
        """Syntactic sugar for getting, for a given toot, the toot stored in 
        database.
        """
        db_toot = self.get(toot)
        
        if db_toot is None:
            raise KeyError(toot)
        
        return db_toot
    
    def get(self, toot):
        """Gets the most recent toot stored in database for a given toot.
        
        If the input toot is from an user, returns the corresponding bot's toot.
        Conversely, returns the corresponding user's toot if a bot's toot is 
        given in input.
        
        If there is more than one results in the database, returns the most
        recent toot.
        """
        if isinstance(toot, dict):
            toot = toot['id']
        
        with contextlib.closing(self.db.cursor()) as cursor:
            cursor.execute("SELECT user_toot, bot_toot FROM `toots` "
            "WHERE bot_toot = :toot OR user_toot = :toot "
            "ORDER BY date DESC", { 'toot' : toot })
            
            row = cursor.fetchone()
        
        if row is None:
            return row
    
        user_toot, bot_toot = row
        
        db_toot = bot_toot if user_toot == toot else user_toot
        
        return db_toot
    
    def add(self, bot_toot, user_toot):
        """Stores a new pair of toots in the database.
        """
        with self.db as cursor:
            cursor.execute("""INSERT INTO `toots` (user_toot, bot_toot, date) 
            VALUES (
                :user_toot,
                :bot_toot,
                :date
            )""", { 
                'user_toot' : user_toot.id, 
                'bot_toot' : bot_toot.id, 
                'date' : bot_toot.created_at.timestamp()
            })
