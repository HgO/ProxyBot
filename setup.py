#!/usr/bin/env python
# -*- coding: utf-8 -*-
from setuptools import setup, find_packages

import proxybot

setup(
    version=proxybot.__version__,
    packages=find_packages(), 
    package_data={ '' : [ 'locales/*.json', 'schemas.sql' ] }
)
