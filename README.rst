Proxybot
======

.. image:: https://img.shields.io/badge/License-AGPL%20v3-blue.svg
    :target: https://www.gnu.org/licenses/agpl-3.0
    :alt: License: AGPL v3

A bot that serves as a proxy for admins of a Mastodon instance.

Install
-------

Install as follow::

    pip3 install --user git+https://framagit.org/HgO/ProxyBot.git

    # download the config file template
    curl -L -o config.ini "https://framagit.org/HgO/ProxyBot/raw/master/config.ini"

    # then tweak the config file (see below)

Usage
-----

``proxybot`` will wait for requests from Mastodon.

Admins can control the bot by sending commands to it. Here are the available commands:

1. !help gives some information on how to use the commands 
2. !toot writes a toot for the admin
3. !reply writes a reply to a toot sent by an user
4. !boost, !unboost, !fav and !unfav do the corresponding actions

In addition, each time an user mentions the bot in a conversation, the message 
will be forwarded to the admins in a direct conversation. They can then send a 
reply with the dedicated !reply command.

In order to prevent other users to be mentionned in the conversation, the first 
character of mentions is replaced by an ``&``. For instance, 
@test@mastodon.social would become &test@mastodon.social.

Conversely, admins can use the '&' character instead of '@' to mention users.
They will be converted back to real mentions by the bot. 

If a toot sent by an user is longer than the character limit on your instance,
then the bot will split the message into smaller toots to fit that limit.

**Tips:** If you plan to run proxybot on a server, you can use ``nohup`` to avoid the program to be killed after logging out::

    nohup parbot &

Configuration file
^^^^^^^^^^^^^^^^^^

The script will look for a ``config.ini`` file in the current directory,
unless you set the ``CONFIG_FILE`` environment variable to a valid path::

    CONFIG_FILE=/etc/proxybot.ini proxybot

    # or via export
    export CONFIG_FILE=/etc/proxybot.ini
    proxybot

Configuration
-------------

You will need to provide an access token in order to connect to Mastodon.
In the ``mastodon`` section, you can indicate the file containing that token 
with ``access_token``.
If such a file doesn't exist, then the program will ask you your credentials to 
register the app to Mastodon.
This operation won't store your credentials, as this is critical information.
Instead, it will generate an access token and store it in the file specified in 
``access_token``.

It is possible to change the visibility of the toots sent via the ``!toot`` 
command. By default, they are sent in public mode.
For instance, ``visibility = unlisted`` would avoid spamming the global 
timeline.

Development
-----------

To have a working copy of the project, simply run the following::

    git clone https://framagit.org/HgO/ProxyBot.git
    pip3 install --user -e .
    # alternatively, run `pip3 install -e .` within a virtualenvironment

Then start hacking!
